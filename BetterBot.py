#!/usr/bin/env python3
# Robot class to manage robot movement functions.
# Should be forked by individuals for additional robot functions. 
# All distance units in inches and rotation units in deg
# Created by Josh 2-23-20 

# --[Supported functions / peripherals]--
# Sensors: Gyro, Ultrasonic
# Motors: Med, Large 

# Version 0.4 - [02/29/2020 00:54] - Nothing special:
#  Changed ultrasonic support to inches from cm
#  Added ecoder return for med motor
#  REMOVED need for port definitions for sensor setup functions
#
# Version 0.3 - [02/28/2020 20:50] - Peripherals: 
#  Added Med motor suport
#  Added Ultrasonic support
#  Added additional turnLeft and turnRight functions
#  Changed turnGy to accept neg values for turing opposite directions
# 
# Version 0.2 - [02/23/2020 21:06] - OK now its working:
#  Added gyro suport
#  Added turing using gyro
#
# Version 0.1 - [02/23/2020 11:59] - Start:
#  Added Robot object
#  Added drive suport
#  


import ev3dev2.motor
import ev3dev2.sensor.lego
import math
from time import sleep


class Robot:

    # Creates Robot object setting basic perameters
    def __init__(self, rightMotorPort, leftMotorPort, wheelDiameter, wheelSpace):
        self.driveMotors = ev3dev2.motor.MoveSteering(rightMotorPort, leftMotorPort)
        self.wheelDiameter = wheelDiameter
        self.wheelSpace = wheelSpace
        self.wheelCircumference = wheelDiameter * math.pi
        self.turnCircumference = wheelSpace * math.pi
        self.speedLimit = 1
        self.postMoveTime = 0.1

    # sets up med motor option for Robot
    def setMedMotor(self, mMotorPort):
        self.medMotor = ev3dev2.motor.MediumMotor(mMotorPort)

    # makes med motor do set rotations
    def medMove_rot(self,speed,rot):
        self.medMotor.on_for_rotations(ev3dev2.motor.SpeedDPS(speed),rot,brake=True,block=True)

    def medGo(self,speed):
        self.medMotor.on(ev3dev2.motor.SpeedDPS(speed),brake=True,block=False)

    def medStop(self):
        self.medMotor.off(brake=True)

    def medMotor_pos(self):
        return self.medMotor.position

    def medMove_pos(self,speed,pos):
        self.medMotor.on_to_position(ev3dev2.motor.SpeedDPS(speed),pos,brake=True,block=True)
    
    # sets up gyro option for Robot
    def setGyro(self):
        self.gyro = ev3dev2.sensor.lego.GyroSensor()
        self.initialAngle = self.gyro.angle

    # sets up US Sensor option for Robot
    def setUltraSonic(self):
        self.ultraSonic = ev3dev2.sensor.lego.UltrasonicSensor()

    # gets Ultrasonic Distance
    def getUSDistance_in(self):
        return self.ultraSonic.distance_inches
    
    # sets universal speed limit (int between 0 and 1 representing %) for Robot used in movement functions. 
    def setSpeedLimit(self, speedLimit):
        self.speedLimit = speedLimit

    # sets how much time will be taken after every move before continuing to next (seconds)
    def setPostMoveTime(self, postMoveTime):
        self.postMoveTime = postMoveTime
    
    # Moves robot forwards by 'inch' based on method of rotation
    def move_Rot(self, inch): #FIX: unclear name; might make people think to enter number of rotations
        self.driveMotors.on_for_rotations(0, ev3dev2.motor.SpeedDPS(360*self.speedLimit), inch/self.wheelCircumference, brake=True, block=True)
        sleep(self.postMoveTime)

    # REQUIRES GYRO (duh) - turns robot by 'deg' based on method of gyro reading 
    # FIX: I want this to be more concise, is that possible?
    def turnGy(self, deg):
        if (deg > 0):
            beforeTurn = self.gyro.angle #getting inital gyro reading
            self.driveMotors.on(100, ev3dev2.motor.SpeedDPS(360*self.speedLimit)) #start turning
            while(abs((self.gyro.angle - beforeTurn)-deg) > 30): #wait until robot is within 30deg of desired mark
                sleep(0.0001)
            self.driveMotors.on(100, ev3dev2.motor.SpeedDPS(360*self.speedLimit*0.5)) #turn at halfspeed
            while(abs((self.gyro.angle - beforeTurn)-deg) > 0): #wait until within 1deg tolerance
                sleep(0.00000001)
            self.driveMotors.off(brake=True) #Full stop
        else:
            beforeTurn = self.gyro.angle #getting inital gyro reading
            self.driveMotors.on(-100, ev3dev2.motor.SpeedDPS(360*self.speedLimit)) #start turning
            while(abs((self.gyro.angle - beforeTurn)-deg) > 30): #wait until robot is within 30deg of desired mark
                sleep(0.0001)
            self.driveMotors.on(-100, ev3dev2.motor.SpeedDPS(360*self.speedLimit*0.5)) #turn at halfspeed
            while(abs((self.gyro.angle - beforeTurn)-deg) > 0): #wait until within 1deg tolerance
                sleep(0.00000001)
            self.driveMotors.off(brake=True) #Full stop

        # Debug 
        #print(gyro.angle - beforeTurn)
        #print((gy.angle - beforeTurn)-deg)) 

        sleep(self.postMoveTime)
    # turnleft gy
    def turnleftGy(self, deg):
        self.turnGy(-1*deg)

    # turn right gy
    def turnRightGy(self, deg):
        self.turnGy(deg)
    
    #Stay on
    def go(self):
        self.driveMotors.on(0,ev3dev2.motor.SpeedDPS(360*self.speedLimit))

    #Brake
    def stop(self):
        self.driveMotors.off(brake=True)
